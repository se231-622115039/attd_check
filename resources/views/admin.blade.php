<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Student ID</th>
                <th scope="col">Feb2</th>
                <th scope="col">Feb9</th>
                <th scope="col">Feb16</th>
                <th scope="col">Feb23</th>
                <th scope="col">Mar2</th>
                <th scope="col">Mar9</th> 
                <th scope="col">Mar16</th> 
                <th scope="col">Total</th>
            </tr> 
        </thead>
        <tbody>
            @foreach ($data as $item) 
            <tr>
                <th>{{$item->no}}</th>
                <td>{{$item->student_id}}</td>
                <td>{{$item->Feb2}}</td>
                <td>{{$item->Feb9}}</td>
                <td>{{$item->Feb16}}</td>
                <td>{{$item->Feb23}}</td>
                <td>{{$item->Mar2}}</td>
                <td>{{$item->Mar9}}</td>
                <td>{{$item->Mar16}}</td>
                <td>{{$item->Feb2 + $item->Feb9 + $item->Feb16 + $item->Feb23 + $item->Mar2 + $item->Mar9 + $item->Mar16}}</td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>